# CKEditor CodeTag

This module adds support for inline code blocks in CKEditor.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/codetag).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/codetag).


## Table of contents

- Requirements
- Installation
- Basic Usage
- Configuration
- Maintainers


## Requirements

This module requires the following modules:

- [Acquia Site Studio](https://github.com/acquia/cohesion)
- [Context](https://www.drupal.org/project/context)

It also requires the core CKEditor module and the CodeTag plugin from
CKEditor.com.


## Installation

1. Download the CodeTag plugin from https://ckeditor.com/cke4/addon/codeTag
2. Place the plugin folder in the root libraries folder (/libraries/codetag).
   If the /libraries folder is not present in your docroot, create one.
3. Enable CodeTag in the Drupal admin.
4. Configure your WYSIWYG toolbar to include the button.


## Basic usage

Drag the CodeTag button into the active toolbar. There is no configuration;
the button should appear in your editor after you save the changes.

Note that your filter format must support the use of the code tag under allowed
tags as well, if using anything other than Full HTML.


## Configuration

The module currently provides no configuration options.


## Maintainers

- Kevin Quillen - [kevinquillen](https://www.drupal.org/u/kevinquillen)
- João Ventura - [jcnventura](https://www.drupal.org/u/jcnventura)
- Kent Richards - [kentr](https://www.drupal.org/u/kentr)
